package ch.supsi.webapp.server;

public class Content
{
	int length = 0;
	byte[] content;
	boolean found;
	
	public Content(byte[] content, boolean found){
		this.found = found;
		this.content = content;
		this.length = content.length;
	}
}